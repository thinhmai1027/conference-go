import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):

    headers = {"Authorization": PEXELS_API_KEY}

    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}, USA", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    the_json = response.json()
    lat = the_json[0]["lat"]
    lon = the_json[0]["lon"]

    return lat, lon


def get_weather_data(city, state):
    lat, lon = get_lat_lon(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "unit": "imperial",
    }

    response = requests.get(url, params=params)
    the_json = response.json()

    return {
        "description": the_json["weather"][0]["description"],
        "temp": the_json["main"]["temp"],
    }
